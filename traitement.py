"""
Script de séparation des informations de descriptions

aurelien.clairais@cerema.fr
24/05/2022
"""

import json
import re

# Correspondance entre les champs du geojson final et les titres des parties de la description
corresp = {
    "Gestionnaire": "gestionnaire",
    "Linéaire concerné": "longueur",
    "Localisation": "localisation",
    "Année de mise en service": "mise_en_service",
    "Type de mesure": "type",
    "Remarques": "remarques",
    "Remarque": "remarques"
}

# Ouverture du fichier initial
with open("observatoire_des_mesures.geojson", encoding="UTF-8") as json_file:
    in_dict = json.load(json_file)

# Pattern pour l'expression régulière de la description
desc_pattern = "\*\*([A-Za-z\séè\s']*) : \*\* ([A-Za-z0-9()\s\[\]éèà,ç/\-\+&;,\s']*)"
# Initialisation
types = []
del_feats = []
other_feats = []

# On recherche ici si certaines entités n'ont pas de nom ou de description
def validation(in_dict):
    val_res = {"Point": {"no_desc": 0, "no_name": 0},
               "MultiLineString": {"no_desc": 0, "no_name": 0},
               "LineString": {"no_desc": 0, "no_name": 0}}
    valide = True
    for feat in in_dict['features']:
        if not('name' in feat['properties'].keys()):
            val_res[feat["geometry"]["type"]]["no_name"] += 1
            valide = False
        if not('description' in feat['properties'].keys()):  
            val_res[feat["geometry"]["type"]]["no_desc"] += 1
            valide = False
    
    if not valide:
        print(f'Points : sans nom {val_res["Point"]["no_name"]} / sans description {val_res["Point"]["no_desc"]}')
        print(f'Lignes : sans nom {val_res["MultiLineString"]["no_name"] + val_res["LineString"]["no_name"]} / sans description {val_res["MultiLineString"]["no_desc"] + val_res["LineString"]["no_desc"]}')

    return valide

# Validation
if not(validation(in_dict)):
    raise ValueError("Il y a des éléments sans nom/description")

            # On boucle sur tous les éléments géométriques
for feat in in_dict['features']:
    # on traite ici les points
    if feat["geometry"]["type"] != "Point":
        # Gestion de la descrition avec l'expression régulière
        desc = feat['properties']['description']
        desc_res = re.findall(desc_pattern, desc)

        # traitement de la correspondance des catégories
        for item in desc_res:
            feat['properties'][corresp[item[0]]] = item[1]
            if corresp[item[0]] == "type" and not(item[1] in types):
                types.append(item[1])

        # Récupération du lien pour "En savoir plus"
        link = re.findall("\[\[([^^]*)\]\]", desc)
        if len(link) > 0:
            feat['properties']['lien'] = link[0]

        del feat['properties']['description']

    # Si ce n'est pas un point on le traite après
    else:
        other_feats.append(feat)

# Traitement des linéaires
for feat in other_feats:
    desc = feat['properties']['description']
    desc_res = re.findall(desc_pattern, desc)
    for item in desc_res:
        # Détection des doublons
        if item[0] in corresp.keys() and corresp[item[0]] == "type" and item[1] in types:
            # suppression du point lorsqu'une ligne existe
            del_feats.append(feat)
            continue
        if item[0] in corresp.keys():
            feat['properties'][corresp[item[0]]] = item[1]
        else:
            feat['properties'][item[0]] = item[1]
        link = re.findall("\[\[([^^]*)\]\]", desc)
        if len(link) > 0:
            feat['properties']['lien'] = link[0]

    del feat['properties']['description']

for feat in del_feats:
    in_dict["features"].remove(feat)
# Export
with open("final_observatoire_regulation.geojson", "w") as outfile:
    json.dump(in_dict, outfile)
