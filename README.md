# Donnees Observatoire Regulations

Export et mise en forme des données de l'observatoire des mesures de régulation du trafic
Source : https://umap.openstreetmap.fr/fr/map/observatoire-des-mesures_393162

Carte aussi visible sur le site régulation intélligente du Cerema :
https://www.cerema.fr/fr/activites/mobilites/systemes-transports-intelligents-trafics-regulation/regulation-trafics/gestion-regulation-intelligentes-trafics

## Utilisation

Le fichier issu de la carte Umap est `observatoire_des_mesures.geojson`.
Le script de traitement est `traitement.py`. Les données finales sont dans `final_observatoire_regulation.geojson`.

## Contributeurs

### Actuels

- Aurélien CLAIRAIS - Cerema Centre-Est

### Anciens

- Nicolas Pelé

### Pour contribuer - Emettre une remarque

aurelien.clairais@cerema.fr

## License
[CeCILL-C](http://www.cecill.info/licences/Licence_CeCILL-C_V1-fr.html)

## Statut du Projet

Le projet sera amené à changer et s'affranchir de Umap. Le fichier geojson final devrait garder le même nom et le lien devrait rester identique quoiqu'il advienne de la méthode de recueil.
